FROM openjdk:8-alpine

COPY Example.java /root/Example.java

RUN javac /root/Example.java

WORKDIR "/root"
CMD ["java", "Example"]